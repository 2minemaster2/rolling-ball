﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;
    public int neededCount;

    // is called before rendering a frame
    void Start ()
	{
        rb = GetComponent<Rigidbody>();
        count = 0;
        neededCount = 21;
        setCountText();
        winText.text = "";
    }
	
	// is called just before any physics movements
	void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        
        this.speed = 10;

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString() + "/" + neededCount;
        if (count >= neededCount)
        {
            winText.text = "You Win!";
        }
    }
}
