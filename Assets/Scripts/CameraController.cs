﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    public Vector3 offset;
    public Vector3 oldLocation;

	// Use this for initialization
	void Start () {
        offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = player.transform.position + offset;
        if (Input.GetKeyDown("f"))
        {
            transform.Rotate(new Vector3(0, 0, -180));
        }
        if (Input.GetKeyUp("f"))
        {
            transform.Rotate(new Vector3(0, 0, 180));
        }
    }
}
